#!/bin/bash
ZOLA_TAG="ghcr.io/getzola/zola:v0.15.3"
if [ "$1" == "init" ]
then
    docker run -it --rm -u "$(id -u):$(id -g)" -v $PWD:/app --workdir /app "$ZOLA_TAG" $*
elif [ "$1" == "serve" ]
then
    docker run -it --rm -u "$(id -u):$(id -g)" -v $PWD:/app --workdir /app -p 8888:8888 "$ZOLA_TAG" serve --interface 0.0.0.0 --port 8888 --base-url localhost
else
    docker run -u "$(id -u):$(id -g)" -v $PWD:/app --workdir /app "$ZOLA_TAG" $*
fi