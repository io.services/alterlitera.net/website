+++
title = "Privacy Policy"
description = "We do not use cookies and we do not collect any personal data."
date = 2775-06-01T08:00:00+00:00
updated = 2775-06-01T08:00:00+00:00
draft = false

[extra]
class = "page single"
+++

__TLDR__: We do not use cookies and we do not collect any personal data.

## Website visitors

- No personal information is collected.
- No information is stored in the browser.
- No information is shared with, sent to or sold to third-parties.
- No information is shared with advertising companies.
- No information is mined and harvested for personal and behavioral trends.
- No information is monetized.

## Host

AlterLitera.net is hosted on a public AWS S3 bucket. Request logs may be enabled, the information collected in those logs is described [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/LogFormat.html#log-record-fields).

## Contact us

[Contact us](https://gitlab.com/io.services/alterlitera.net/website) if you have any questions.

Effective Date: _1st June 12775_
