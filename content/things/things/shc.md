+++
title = "Secular Holocene Calendar"
description = "What year is it again?"
date = 2775-06-15T16:23:00+04:00
updated = 2775-06-15T16:23:00+04:00
template = "things/page.html"
sort_by = "weight"
weight = 1
draft = false

[extra]

toc = true
top = false
+++

## Terms

Here are the terms, as they are meant in this article and not how they may be understood in other contexts.

### Secular

Secular·ism is the specialization of impartiality towards dogma. Something is secular is if it impervious to dogma. For instance, to the question: Should we burn the witch? The secular actor, provided no additionnal context is available, will reply: No.

* Impartial·ity - Making fair and objective choices. Thus free of bias, free of prejudice, and free of fraud.

  * Bias - Unequal inclination towards or against something based on status, rights, or opportunities.
  * Prejudice - Fallacious opinion

* Dogma - A belief or set of beliefs shielded from doubt.
* Actor - Abstraction of something that can interact with a given context. In this context, likely a person or an organisation. - See [actor model](https://en.wikipedia.org/wiki/Actor_model).

### Holocene calendar

Wikipedia has a good explanation [here](https://en.wikipedia.org/wiki/Holocene_calendar), and archived [here](https://web.archive.org/web/20220601220245/https://en.wikipedia.org/wiki/Holocene_calendar), and [here](https://archive.ph/JkMSI).

## Puting it all together - Secular Holocene Calendar

The holocene calendar on its own has a problem. It is not secular as its year revolves around the supposed birth of a religious figure.

What would a secular calendar use as a year then? Looking back at the history of the calendar itself, before Gregy, the calendar was roman, the previous version being commissioned by the real JC. So let's just use that instead right? It's unfortunately not that simple as the years were denominated using whoever were consuls of the senate for that year. Fortunately for us however a contemporary roman did some math and history to figure out an approximative date to the start of the calendar - the founding of Rome.

Ab Urbe Condita, often abbreviated as AUC, means "From The Founding Of The City". It refers to the year of the founding of Rome, which was the Epoch used in Ancient Rome.

Using AUC the year would be 2775 (2022 CE/AD). The formula to convert from CE to AUC being: add 753 years.
Combining with the holocene calendar, which also adds 10 000 years...

The current year as denominated by the Secular Holocene Calendar is 12775. Now that's living in the future.

## Links

* [https://aburbecondita.com/](https://aburbecondita.com/)
* [https://en.wikipedia.org/wiki/Ab_urbe_condita](https://en.wikipedia.org/wiki/Ab_urbe_condita)
