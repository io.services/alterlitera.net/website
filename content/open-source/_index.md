+++
title = "Open Source"
description = "This website is powered by open source projects."
date = 2775-06-01T08:00:00+00:00
updated = 2775-06-01T08:00:00+00:00
draft = false

[extra]
class = "page single"
+++

AlterLitera.net is powered by these open source projects.

This list does not constitute an endorsement by - or of - the following projects. AlterLitera.net is not affiliated or associated with the projects mentionned in this list.

## Projects

- <a href="https://www.getzola.org/">Zola (static site generator) </a>
- <a href="https://github.com/aaranxu/adidoks">AdiDoks (zola theme)</a>
- <a href="https://www.rust-lang.org/">Rust (programming language)</a>
